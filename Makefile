# ------------------------------------------------------------------------------
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License.
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
#  Custom variables
# ------------------------------------------------------------------------------

INPUT = parser

OUTPUT = txt2svg

#  -----------------------------------------------------------------------------
#    Flags
#  -----------------------------------------------------------------------------

# Use GNU syntax
GNU?=yes

ifeq ($(GNU),yes)
CPPFLAGS += -std=gnu++14
else
CPPFLAGS += -std=c++1y
endif

# Optimize code with -O2
OPTIMIZE?=yes

ifeq ($(OPTIMIZE),yes)
CPPFLAGS += -O2
endif

# Set warning flags
WARNING?=no

ifeq ($(WARNING),yes)
CPPFLAGS += -Wall
endif

# Set warning as error flags
ERROR?=no

ifeq ($(ERROR),yes)
CPPFLAGS += -Werror
endif

# ------------------------------------------------------------------------------
#  Make
# ------------------------------------------------------------------------------

all: checkdirs bison flex link clean

link:
	$(call colorecho,"Compile project")
	g++ $(CPPFLAGS) -c forms.cpp -o forms.o -lfl
	g++ $(CPPFLAGS) -c lex.yy.c -o lex.yy.o -lfl
	g++ $(CPPFLAGS) -c $(INPUT).tab.c -o $(INPUT).tab.o
	g++ $(CPPFLAGS) -o $(OUTPUT) lex.yy.o $(INPUT).tab.o -lfl forms.o

bison:
	$(call colorecho,"Start bison compiler")
	bison -d $(INPUT).y

flex:
	$(call colorecho,"Start flex compiler")
	flex $(INPUT).lex

run: all
	$(call colorecho,"Launch project")
	./$(OUTPUT)

	$(call colorecho,"Read output")
	cat output.svg

checkdirs: $(BUILD_DIR)
	$(call colorecho,"Check variables")
	@echo \
	  " GNU      : $(GNU)\n" \
	  "OPTIMIZE : $(OPTIMIZE)\n" \
	  "WARNING  : $(WARNING)\n" \
	  "ERROR    : $(ERROR)\n" \
	  "FLAGS    : $(CPPFLAGS)\n" \

clean:
	$(call colorecho,"Clean project")
	rm -f lex.yy.c lex.yy.o $(INPUT).tab.c $(INPUT).tab.c $(INPUT).tab.o $(INPUT).tab.h forms.o

clean-all: clean
	rm -f $(OUTPUT)

# ------------------------------------------------------------------------------
#  Define
# ------------------------------------------------------------------------------

define colorecho
    @tput bold
    @tput setaf 2
    @echo ">>>" $1
    @tput sgr0
endef
