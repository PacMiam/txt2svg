/* --------------------------------------------------------------------------
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * -------------------------------------------------------------------------- */

#ifndef FORMS__HPP
#define FORMS__HPP

/* --------------------------------------------------------------------------
 *  Modules
 * -------------------------------------------------------------------------- */

#include <string>
#include <map>

/* --------------------------------------------------------------------------
 *  Class
 * -------------------------------------------------------------------------- */

class Form {

    public:
        std::map<std::string, std::string> transforms;
        std::map<std::string, std::string> attributes;

        // Constructor
        Form();
        Form(const int &, const int &);

        const std::string get_attribute(const std::string & key) {
            if(attributes.find(key) != attributes.end())
                return attributes[key];

            return NULL;
        }
        const std::string get_transform(const std::string & key) {
            if(transforms.find(key) != transforms.end())
                return transforms[key];

            return NULL;
        }

        virtual const std::string name() { return "form"; };
};

class FormLine : public Form {

    public:
        // Constructor
        FormLine();
        FormLine(const int &, const int &, const int &, const int &);

        virtual const std::string name() { return "line"; };
};

class FormText : public Form {

    public:
        std::string text;

        // Constructor
        FormText();
        FormText(const int &, const int &, const std::string &);

        const std::string get_text() { return text; };

        virtual const std::string name() { return "text"; };
};

class FormCircle : public Form {

    public:
        // Constructor
        FormCircle();
        FormCircle(const int &, const int &, const int &);

        virtual const std::string name() { return "circle"; };
};

class FormRectangle : public Form {

    public:
        // Constructor
        FormRectangle();
        FormRectangle(const int &, const int &, const int &, const int &);

        virtual const std::string name() { return "rect"; };
};

#endif // FORMS__HPP
