/* --------------------------------------------------------------------------
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * -------------------------------------------------------------------------- */

%require "2.4"

%{
    /* ---------------------------------------
     *  Modules
     * --------------------------------------- */

    #include <stdio.h>
    #include <stdlib.h>

    #include <algorithm>
    #include <typeinfo>
    #include <iostream>
    #include <fstream>
    #include <string>
    #include <vector>
    #include <array>
    #include <map>

    #include "forms.hpp"

    /* ---------------------------------------
     *  Bison
     * --------------------------------------- */

    int yylex(void);
    int yyparse(void);

    void yyerror(const std::string);

    /* ---------------------------------------
     *  SVG header
     * --------------------------------------- */

    int width;
    int height;

    std::string title;
    std::string description;

    /* ---------------------------------------
     *  Forms storage
     * --------------------------------------- */

    // Keep latest Form index
    int current_form = -1;

    // Store Form objects
    std::vector<Form*> store_forms;

    // Store Form variables (eg. T1) with Form index from store_forms
    std::map<std::string, int> store_index;

    // Transformation's list
    std::array<std::string, 3> transforms_list = {{
        "scale", "rotation", "translation" }};

    // Specific syntax
    std::map<std::string, std::string> syntax = {
        { "rayon", "r" },
    };

    /* ---------------------------------------
     *  Functions
     * --------------------------------------- */

    void append_data(
        const int & index, const std::string & key, const std::string & value) {
        /*  Append a value for a specific key
         *
         *  Parameters
         *  ----------
         *  index : int
         *      Form index in store_index
         *  key : std::string
         *      Map key for attributes/transforms list
         *  value : std::string
         *      New map value for key
         */

        std::string new_key = key;

        // Check a specific syntax for curent key
        if(syntax.find(key) != syntax.end())
            new_key = syntax[key];

        // This is a transformation
        if(std::find(transforms_list.begin(), transforms_list.end(),
            new_key) != transforms_list.end())
            store_forms[index]->transforms[new_key] = value;

        // This is an attribute
        else
            store_forms[index]->attributes[new_key] = value;
    }

    std::string get_data(const int & index, const std::string & key) {
        /*  Return a value from a specific key
         *
         *  Parameters
         *  ----------
         *  index : int
         *      Form index in store_index
         *  key : std::string
         *      Map key for attributes/transforms list
         *
         *  Returns
         *  -------
         *  std::string or NULL
         *      Request value from specific key
         */

        // This is a transformation
        if(std::find(transforms_list.begin(), transforms_list.end(),
            key) != transforms_list.end())
            return(store_forms[index]->get_transform(key));

        // This is an attribute
        else
            return(store_forms[index]->get_attribute(key));
    }
%}

%union {
    int t_int;
    char* t_str;
    float t_float;
}

/* --------------------------------------------------------------------------
 *  Type
 * -------------------------------------------------------------------------- */

%token<t_int> Integer
%token<t_float> Float

%token<t_str> Id
%token<t_str> String
%token<t_str> Message
%token<t_str> Position

/* --------------------------------------------------------------------------
 *  Tokens
 * -------------------------------------------------------------------------- */

// Parenthesis ()
%token LeftParenthesis RightParenthesis

// Brackets []
%token LeftBracket RightBracket

// Braces {}
%token LeftBrace RightBrace

// Separators ; . , " \n
%token Separator Dot Comma Quote EOL

// Math ° =
%token Degree Equal

// Identifiers
%token Canvas Title Description Rectangle Circle Line Text Attribute

/* --------------------------------------------------------------------------
 *  Types
 * -------------------------------------------------------------------------- */

%type<t_str> Canvas
%type<t_str> Title
%type<t_str> Description
%type<t_str> Attribute

/* --------------------------------------------------------------------------
 *  Input
 * -------------------------------------------------------------------------- */

%start Interpreter
%%

    /* ----------------------------------------------------------------------
     *  Interpreter
     * ---------------------------------------------------------------------- */

    Interpreter: /* empty */
        | Forms EOL {}
        | Forms LeftBrace Attributes RightBrace EOL {}
        | Arguments EOL {}
        | Arguments Separator EOL {}
        | Attributes Separator {}
        | Attributes Separator EOL {}
        | Interpreter Forms EOL {}
        | Interpreter Forms LeftBrace Attributes RightBrace EOL {}
        | Interpreter Arguments EOL {}
        | Interpreter Arguments Separator EOL {}
        | Interpreter Attributes Separator {}
        | Interpreter Attributes Separator EOL {}
        ;

    /* ----------------------------------------------------------------------
     *  Parser
     * ---------------------------------------------------------------------- */

    Arguments: /* empty */
        | Canvas LeftParenthesis Integer Comma Integer RightParenthesis {
            /*  Generate canvas header
             *
             *  => canvas(width, height) */

            width = $3;
            height = $5;
        }
        | Title LeftParenthesis Message RightParenthesis {
            /*  Generate title header
             *
             *  => title(text) */

            title = $3;

            // Remove quotes
            title.erase(std::remove(title.begin(), title.end(), '"'),
                title.end());
            // Replace EndOfLine by spaces
            std::replace(title.begin(), title.end(), '\n', ' ');
        }
        | Description LeftParenthesis Message RightParenthesis {
            /*  Generate description header
             *
             *  => description(text) */

            description = $3;

            // Remove quotes
            description.erase(std::remove(description.begin(),
                description.end(), '"'), description.end());
            // Replace EndOfLine by spaces
            std::replace(description.begin(), description.end(), '\n', ' ');
        }
        ;

    Forms: /* empty */
        | LeftBracket Rectangle Comma LeftParenthesis Integer Comma Integer
            RightParenthesis Comma Integer Comma Integer RightBracket {
            /*  Generate a Rectangle
             *
             *  => [RECT, (x, y), width, height] */

            FormRectangle* form = new FormRectangle($5, $7, $10, $12);

            // Store rectangle
            store_forms.push_back(form);

            // Update index
            current_form++;
        }
        | LeftBracket Rectangle RightBracket {
            /*  Generate a Rectangle
             *
             *  => [RECT] */

            FormRectangle* form = new FormRectangle();

            // Store rectangle
            store_forms.push_back(form);

            // Update index
            current_form++;
        }
        | LeftBracket Circle Comma LeftParenthesis Integer Comma Integer
            RightParenthesis Comma Integer RightBracket {
            /*  Generate a Circle
             *
             *  => [CERCLE, (x, y), radius] */

            FormCircle* form = new FormCircle($5, $7, $10);

            // Store circle
            store_forms.push_back(form);

            // Update index
            current_form++;
        }
        | LeftBracket Line Comma LeftParenthesis Integer Comma Integer
            RightParenthesis Comma LeftParenthesis Integer Comma Integer
            RightParenthesis RightBracket {
            /*  Generate a Line
             *
             *  => [LIGNE, (x1, y1), (x2, y2)] */

            FormLine* form = new FormLine($5, $7, $11, $13);

            // Store line
            store_forms.push_back(form);

            // Update index
            current_form++;
        }
        | LeftBracket Text Comma LeftParenthesis Integer Comma Integer
            RightParenthesis Comma Message RightBracket {
            /*  Generate a Text
             *
             *  => [TEXT, (x1, y1), text] */

            std::string text = $10;

            // Remove quotes
            text.erase(std::remove(text.begin(), text.end(), '"'), text.end());

            FormText* form = new FormText($5, $7, text);

            // Store text
            store_forms.push_back(form);

            // Update index
            current_form++;
        }
        | Id LeftBracket Rectangle Comma LeftParenthesis Integer Comma Integer
            RightParenthesis Comma Integer Comma Integer RightBracket {
            /*  Generate a Rectangle
             *
             *  => ID [RECT, (x, y), width, height] */

            FormRectangle* form = new FormRectangle($6, $8, $11, $13);

            // Store rectangle
            store_forms.push_back(form);

            // Update index
            current_form++;

            // Store ID
            store_index[$1] = current_form;
        }
        | Id LeftBracket Rectangle Comma LeftParenthesis Integer Comma Integer
            RightParenthesis RightBracket {
            /*  Generate a Rectangle
             *
             *  => ID [RECT, (x, y)] */

            FormRectangle* form = new FormRectangle($6, $8, 0, 0);

            // Store rectangle
            store_forms.push_back(form);

            // Update index
            current_form++;

            // Store ID
            store_index[$1] = current_form;
        }
        | Id LeftBracket Rectangle RightBracket {
            /*  Generate a Rectangle
             *
             *  => ID [RECT] */

            FormRectangle* form = new FormRectangle();

            // Store rectangle
            store_forms.push_back(form);

            // Update index
            current_form++;

            // Store ID
            store_index[$1] = current_form;
        }
        | Id Equal LeftBracket Circle Comma LeftParenthesis Integer Comma
            Integer RightParenthesis Comma Integer RightBracket {
            /*  Generate a Circle
             *
             *  => ID = [CERCLE, (x, y), radius] */

            FormCircle* form = new FormCircle($7, $9, $12);

            // Store circle
            store_forms.push_back(form);

            // Update index
            current_form++;

            // Store ID
            store_index[$1] = current_form;
        }
        | Id Equal LeftBracket Circle Comma LeftParenthesis Integer Comma
            Integer RightParenthesis RightBracket {
            /*  Generate a Circle
             *
             *  => ID = [CERCLE, (x, y)] */

            FormCircle* form = new FormCircle($7, $9, 0);

            // Store circle
            store_forms.push_back(form);

            // Update index
            current_form++;

            // Store ID
            store_index[$1] = current_form;
        }
        | Id LeftBracket Line Comma LeftParenthesis Integer Comma Integer
            RightParenthesis Comma LeftParenthesis Integer Comma Integer
            RightParenthesis RightBracket {
            /*  Generate a Line
             *
             *  => ID [LIGNE, (x1, y1), (x2, y2)] */

            FormLine* form = new FormLine($6, $8, $12, $14);

            // Store line
            store_forms.push_back(form);

            // Update index
            current_form++;

            // Store ID
            store_index[$1] = current_form;
        }
        | Id LeftBracket Line RightBracket {
            /*  Generate a Line
             *
             *  => ID [LIGNE] */

            FormLine* form = new FormLine();

            // Store line
            store_forms.push_back(form);

            // Update index
            current_form++;

            // Store ID
            store_index[$1] = current_form;
        }
        | Id LeftBracket Text Comma LeftParenthesis Integer Comma Integer
            RightParenthesis Comma Message RightBracket {
            /*  Generate a Text
             *
             *  => ID [TEXTE, (x1, y1), text] */

            std::string text = $11;

            // Remove quotes
            text.erase(std::remove(text.begin(), text.end(), '"'), text.end());

            FormText* form = new FormText($6, $8, text);

            // Store text
            store_forms.push_back(form);

            // Update index
            current_form++;

            // Store ID
            store_index[$1] = current_form;
        }
        ;

    Attributes: /* empty */
        | Attribute LeftParenthesis Integer RightParenthesis {
            /*  Generate an attribute
             *
             *  => attribute(int) */

            append_data(current_form, $1, std::to_string($3));
        }
        | Attribute LeftParenthesis Float RightParenthesis {
            /*  Generate an attribute
             *
             *  => attribute(float) */

            append_data(current_form, $1, std::to_string($3));
        }
        | Attribute LeftParenthesis Attribute RightParenthesis {
            /*  Generate an attribute
             *
             *  => attribute(string) */

            append_data(current_form, $1, $3);
        }
        | Attribute Equal Integer Degree {
            /*  Generate an attribute
             *
             *  => attribute = int° */

            append_data(current_form, $1, std::to_string($3));
        }
        | Attribute Equal Float Degree {
            /*  Generate an attribute
             *
             *  => attribute = float° */

            append_data(current_form, $1, std::to_string($3));
        }
        | Attribute Equal Integer {
            /*  Generate an attribute
             *
             *  => attribute = int */

            append_data(current_form, $1, std::to_string($3));
        }
        | Attribute Equal Float {
            /*  Generate an attribute
             *
             *  => attribute = float */

            append_data(current_form, $1, std::to_string($3));
        }
        | Attribute Equal Attribute {
            /*  Generate an attribute
             *
             *  => attribute = string */

            append_data(current_form, $1, $3);
        }
        | Attribute Equal Id Dot Attribute {
            /*  Generate an attribute
             *
             *  => attribute = ID.string */

            if(store_index.find($3) != store_index.end()) {
                // if(get_data(store_index[$3], $5) != NULL)
                    append_data(current_form, $1, get_data(store_index[$3], $5));
            }
        }
        | Id Dot Attribute LeftParenthesis Integer RightParenthesis {
            /*  Generate an attribute
             *
             *  => ID.attribute(int) */

            if(store_index.find($1) != store_index.end())
                append_data(store_index[$1], $3, std::to_string($5));
        }
        | Id Dot Attribute LeftParenthesis Float RightParenthesis {
            /*  Generate an attribute
             *
             *  => ID.attribute(float) */

            if(store_index.find($1) != store_index.end())
                append_data(store_index[$1], $3, std::to_string($5));
        }
        | Id Dot Attribute LeftParenthesis Attribute RightParenthesis {
            /*  Generate an attribute
             *
             *  => ID.attribute(string) */

            if(store_index.find($1) != store_index.end())
                append_data(store_index[$1], $3, $5);
        }
        | Id Dot Attribute Integer Equal LeftParenthesis Integer Comma Integer
            RightParenthesis {
            /*  Generate an attribute
             *
             *  => ID.attributeint = (int, int) */

            std::string attributes = $3;

            std::string first = attributes.substr(
                0, attributes.size() / 2) + std::to_string($4);
            std::string second = attributes.substr(attributes.size() / 2,
                attributes.size() / 2) + std::to_string($4);

            if(store_index.find($1) != store_index.end()) {
                append_data(store_index[$1], first, std::to_string($7));
                append_data(store_index[$1], second, std::to_string($9));
            }
        }
        | Id Dot Attribute Equal Integer {
            /*  Generate an attribute
             *
             *  => ID.attribute = int */

            if(store_index.find($1) != store_index.end())
                append_data(store_index[$1], $3, std::to_string($5));
        }
        | Id Dot Attribute Integer Equal Integer {
            /*  Generate an attribute
             *
             *  => ID.attributeint = int */

            std::string data = $3 + std::to_string($4);

            if(store_index.find($1) != store_index.end())
                append_data(store_index[$1], data, std::to_string($6));
        }
        | Id Dot Attribute Equal Float {
            /*  Generate an attribute
             *
             *  => ID.attribute = float */

            if(store_index.find($1) != store_index.end())
                append_data(store_index[$1], $3, std::to_string($5));
        }
        | Id Dot Attribute Equal Position {
            /*  Generate an attribute
             *
             *  => ID.attribute = "int" */

            std::string data = $5;

            // Remove quotes
            data.erase(std::remove(data.begin(), data.end(), '"'), data.end());

            if(store_index.find($1) != store_index.end())
                append_data(store_index[$1], $3, data);
        }
        | Id Dot Attribute Equal Message {
            /*  Generate an attribute
             *
             *  => ID.attribute = "string" */

            std::string data = $5;

            // Remove quotes
            data.erase(std::remove(data.begin(), data.end(), '"'), data.end());

            if(store_index.find($1) != store_index.end())
                append_data(store_index[$1], $3, data);
        }
        | Id Dot Attribute Equal Attribute {
            /*  Generate an attribute
             *
             *  => ID.attribute = string */

            if(store_index.find($1) != store_index.end())
                append_data(store_index[$1], $3, $5);
        }
        | Id Dot Attribute Equal Attribute LeftParenthesis Integer Comma Integer
            Comma Integer RightParenthesis {
            /*  Generate an attribute
             *
             *  => ID.attribute = RGB(int, int, int) */

            std::string data = "rgb(" + std::to_string($7) + "," + \
                std::to_string($9) + "," + std::to_string($11) + ")";

            if(store_index.find($1) != store_index.end())
                append_data(store_index[$1], $3, data);
        }
        | Id Dot Attribute Equal Id Dot Attribute {
            /*  Generate an attribute
             *
             *  => ID.attribute = ID.attribute */

            if(store_index.find($1) != store_index.end()) {
                if(store_index.find($5) != store_index.end()) {
                    // if(get_data(store_index[$3], $5) != NULL)
                        append_data(
                            store_index[$1], $3, get_data(store_index[$5], $7));
                }
            }
        }
        | Attributes Separator {}
        | Attributes EOL Attributes {}
        ;

%%

/* --------------------------------------------------------------------------
 *  Functions
 * -------------------------------------------------------------------------- */

void yyerror(const std::string message) {
    /*  Show a message when an error occurs
     *
     *  Parameters
     *  ----------
     *  message : std::string
     *      Message to print
     */

    fprintf(stderr, "!! %s\n", message.c_str());
}

int main(void) {
    /*  Main launcher
     */

    // Start parser
    yyparse();

    /* -----------------------------------
     *  Write output
     * ----------------------------------- */

    std::cout << "Generate SVG output" << std::endl;

    std::ofstream output("output.svg");

    output << "<?xml version=\"1.0\" encoding=\"utf-8\"?>" << std::endl;

    // SVG header
    output << "<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" ";
    output << "width=\"" << width << "\" height=\"" << height << "\">";
    output << std::endl;

    // Title
    if(title.length() > 0)
        output << "\t<title>" << title << "</title>" << std::endl;

    // Description
    if(description.length() > 0)
        output << "\t<desc>" << description << "</desc>" << std::endl;

    /* -----------------------------------
     *  Forms
     * ----------------------------------- */

    for(const auto & data : store_forms ) {

        /* -----------------------------------
         *  Rectangle
         * ----------------------------------- */

        if(data->name() == "rect") {
            FormRectangle* form = static_cast<FormRectangle*>(data);

            output << "\t<rect ";

            // Attributes
            for(const auto & element : form->attributes) {
                if(element.first == "remplissage")
                    output << "fill=\"" << element.second << "\" ";
                else
                    output << element.first << "=\"" << element.second << "\" ";
            }

            // Transformations
            if(form->transforms.size() > 0) {
                output << "transform=\"";

                for(const auto & element: form->transforms) {
                    output << element.first << "(" << element.second << ") ";
                }

                output << "\" ";
            }

            output << "/>" << std::endl;
        }

        /* -----------------------------------
         *  Circle
         * ----------------------------------- */

        else if(data->name() == "circle") {
            FormCircle* form = static_cast<FormCircle*>(data);

            output << "\t<circle ";

            // Attributes
            for(const auto & element : form->attributes) {
                if(element.first == "remplissage")
                    output << "fill=\"" << element.second << "\" ";
                else
                    output << element.first << "=\"" << element.second << "\" ";
            }

            // Transformations
            if(form->transforms.size() > 0) {
                output << "transform=\"";

                for(const auto & element: form->transforms) {
                    output << element.first << "(" << element.second << ") ";
                }

                output << "\" ";
            }

            output << "/>" << std::endl;
        }

        /* -----------------------------------
         *  Line
         * ----------------------------------- */

        else if(data->name() == "line") {
            FormLine* form = static_cast<FormLine*>(data);

            output << "\t<line ";

            // Attributes
            for(const auto & element : form->attributes) {
                if(element.first == "couleur")
                    output << "stroke=\"" << element.second << "\" ";
                else
                    output << element.first << "=\"" << element.second << "\" ";
            }

            // Transformations
            if(form->transforms.size() > 0) {
                output << "transform=\"";

                for(const auto & element: form->transforms) {
                    output << element.first << "(" << element.second << ") ";
                }

                output << "\" ";
            }

            output << "/>" << std::endl;
        }

        /* -----------------------------------
         *  Text
         * ----------------------------------- */

        else if(data->name() == "text") {
            FormText* form = static_cast<FormText*>(data);

            output << "\t<text ";

            // Attributes
            for(const auto & element : form->attributes) {
                if(element.first == "couleur")
                    output << "stroke=\"" << element.second << "\" ";
                else
                    output << element.first << "=\"" << element.second << "\" ";
            }

            // Transformations
            if(form->transforms.size() > 0) {
                output << "transform=\"";

                for(const auto & element: form->transforms) {
                    output << element.first << "(" << element.second << ") ";
                }

                output << "\" ";
            }

            // Text
            output << ">" << form->get_text() << "</text>" << std::endl;
        }
    }

    output << "</svg>" << std::endl;

    output.close();

    return EXIT_SUCCESS;
}
