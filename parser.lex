/* --------------------------------------------------------------------------
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * -------------------------------------------------------------------------- */

/* --------------------------------------------------------------------------
 *  Modules
 * -------------------------------------------------------------------------- */

%{
    #include <stdlib.h>

    #include <string>

    #include "parser.tab.h"
%}

/* the manual says "somewhat more optimized" */
%option batch

/* enable scanner to generate debug output. disable this for release versions */
/* %option debug */

/* no support for include files is planned */
%option yywrap nounput

/* enables the use of start condition stacks */
%option stack

/* --------------------------------------------------------------------------
 *  Regex
 * -------------------------------------------------------------------------- */

whitespace  [ \t]*
quote       \"

integer     [0-9]+
real        [0-9]+(\.[0-9*])?

id          [A-Z][0-9]+
attribute   [A-Za-z]+
message     {quote}[^\"]*{quote}
position    {quote}{integer}{quote}

canvas      ([Cc][Aa][Nn][Vv][Aa][Ss])?
title       ([Tt][Ii][Tt][Ll][Ee])?
description ([Dd][Ee][Ss][Cc][Rr][Ii][Pp][Tt][Ii][Oo][Nn])?

rectangle   ([Rr][Ee][Cc][Tt])?
line        ([Ll][Ii][Gn][Nn][Ee])?
circle      ([Cc][Ee][Rr][Cc][Ll][Ee])?
text        ([Tt][Ee][Xx][Tt][Ee])?

/* --------------------------------------------------------------------------
 *  Tokens
 * -------------------------------------------------------------------------- */

%%

{whitespace} {}

"\n" {
    return(EOL);
}

{canvas} {
    return(Canvas);
}

{title} {
    return(Title);
}

{description} {
    return(Description);
}

{rectangle} {
    yylval.t_str = strdup(yytext);
    return(Rectangle);
}

{line} {
    yylval.t_str = strdup(yytext);
    return(Line);
}

{circle} {
    yylval.t_str = strdup(yytext);
    return(Circle);
}

{id} {
    yylval.t_str = strdup(yytext);
    return(Id);
}

{position} {
    yylval.t_str = strdup(yytext);
    return(Position);
}

{quote} {
    yylval.t_str = strdup(yytext);
    return(Quote);
}

{text} {
    yylval.t_str = strdup(yytext);
    return(Text);
}

{message} {
    yylval.t_str = strdup(yytext);
    return(Message);
}

{attribute} {
    yylval.t_str = strdup(yytext);
    return(Attribute);
}

{integer} {
    yylval.t_int = atoi(yytext);
    return(Integer);
}

{real} {
    yylval.t_float = atof(yytext);
    return(Float);
}

"," {
    return(Comma);
}
";" {
    return(Separator);
}
"°" {
    return(Degree);
}
"=" {
    return(Equal);
}
"(" {
    return(LeftParenthesis);
}
")" {
    return(RightParenthesis);
}
"[" {
    return(LeftBracket);
}
"]" {
    return(RightBracket);
}
"{" {
    return(LeftBrace);
}
"}" {
    return(RightBrace);
}
"." {
    return(Dot);
}

