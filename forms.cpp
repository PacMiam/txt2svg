/* --------------------------------------------------------------------------
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * -------------------------------------------------------------------------- */

#include "forms.hpp"

/* --------------------------------------------------------------------------
 *  Forms
 * -------------------------------------------------------------------------- */

Form::Form() {}

Form::Form(const int & x, const int & y) {}

/* --------------------------------------------------------------------------
 *  Forms / Line
 * -------------------------------------------------------------------------- */

FormLine::FormLine() {}

FormLine::FormLine(
    const int & x1, const int & y1, const int & x2, const int & y2) {
    attributes["x1"] = std::to_string(x1);
    attributes["y1"] = std::to_string(y1);
    attributes["x2"] = std::to_string(x2);
    attributes["y2"] = std::to_string(y2);
}

/* --------------------------------------------------------------------------
 *  Forms / Text
 * -------------------------------------------------------------------------- */

FormText::FormText() {}

FormText::FormText(const int & x, const int & y, const std::string & n_text) {
    attributes["x"] = std::to_string(x);
    attributes["y"] = std::to_string(y);

    text = n_text;
}

/* --------------------------------------------------------------------------
 *  Forms / Circle
 * -------------------------------------------------------------------------- */

FormCircle::FormCircle() {}

FormCircle::FormCircle(const int & x, const int & y, const int & radius) {
    attributes["cx"] = std::to_string(x);
    attributes["cy"] = std::to_string(y);
    attributes["r"] = std::to_string(radius);
}

/* --------------------------------------------------------------------------
 *  Forms / Rectangle
 * -------------------------------------------------------------------------- */

FormRectangle::FormRectangle() {}

FormRectangle::FormRectangle(
    const int & x, const int & y, const int & width, const int & height) {
    attributes["x"] = std::to_string(x);
    attributes["y"] = std::to_string(y);
    attributes["width"] = std::to_string(width);
    attributes["height"] = std::to_string(height);
}
