# txt2svg

Projet de compilation pour le Master 1 Informatique de l'université d'Angers. Le
but étant de convertir un fichier texte au format SVG en utilisant **Bison** et
**Flex**.

## Dépendances

Les dépendances suivantes sont requises pour faire fonctionner le projet :

* bison
* coreutils (cat)
* flex
* g++
* make

## Utilisation

### Compilation

Pour compiler le projet, il suffit de lancer la commande suivante à la racine du
projet :

```
$ make
```

Il est possible de spécifier au Makefile de lancer le binaire juste après la
compilation avec la commande suivante :

```
$ make run < [filename]
```

### Lancement

Une fois la compilation réalisée, le binaire _txt2svg_ devrait être présent à la
racine du projet. Pour générer un svg, il suffit de lancer la commande suivante :

```
$ ./txt2svg [filename]
```

Un fichier nommé _output.svg_ sera généré à la racine du projet.

## Exemple

```
canvas(1920,1080)
title("Exemple 1")
description("Cette figure est constituée d’un rectangle,
d’un segment de droite et d’un cercle.")
[RECT,(0,70),100,80]{remplissage=green;
rotation=45°;
translation=30;
}
[LIGNE,(5,5),(250,95)]{couleur=red;}
[CERCLE,(90,80),50]{remplissage=blue;
scale(1.5);}
[TEXTE,(180,60),"Un texte"]{couleur=red;}
```

D'autres exemples sont disponibles dans le dossier _examples_ présent à la
racine du projet.

## Licence

Ce projet est disponible sous licence [GPL3](https://www.gnu.org/licenses/gpl.html).

```
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 3 of the License.
```
